package br.com.devmedia.articles.spring.batch.etl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Classe principal do nosso módulo ETL, responsável pela inicialização da aplicação.
 * A partir desta classe é que o contexto da aplicação é carregado e o sistema é, enfim, executado.
 *
 * Created by pedrobrigatto on 7/1/15.
 */
public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main (String[] arguments) {
        ApplicationContext context = new ClassPathXmlApplicationContext("etlapp-context.xml");
        JobLauncher launcher = (JobLauncher) context.getBean("jobLauncher");
        Job job = (Job) context.getBean("devmedia_job");

        try {
            JobExecution execution = launcher.run(job, new JobParameters());
            logger.info("Execution has finished with exit code " + execution.getExitStatus().getExitCode());
            logger.info("Execution exit description: " + execution.getExitStatus().getExitDescription());

        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }
    }
}
