package br.com.devmedia.articles.spring.batch.etl.loaders.databases;

import br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions.DatabaseException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * Representação genérica de um objeto de acesso a dados, com todos os atributos necessários por qualquer DAO da
 * aplicação para se comunicar com o banco de dados e realizar operações de persistência nele.
 *
 * Created by pedrobrigatto on 6/28/15.
 */
public abstract class Dao<T> {

    private JdbcTemplate jdbcTemplate;

    public abstract T save(T object) throws DatabaseException;

    public abstract List<T> listAll() throws DatabaseException;

    /**
     * Use this method to find a specific object by any field - or a combination of fields - supposed to represent a
     * single tuple in the database.
     *
     * @param criteria The criteria to search for objects in a given table in the database.
     * @return The object of interest, if any matches the criteria.
     */
    public abstract T find(String ... criteria) throws DatabaseException;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
