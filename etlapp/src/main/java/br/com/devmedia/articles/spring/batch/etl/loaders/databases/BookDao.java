package br.com.devmedia.articles.spring.batch.etl.loaders.databases;

import br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions.DatabaseException;
import br.com.devmedia.articles.spring.batch.etl.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.sql.Types;
import java.util.List;

/**
 * Data Access Objects to manage the operations related to books in the database.
 *
 * Created by pedrobrigatto on 6/27/15.
 */
public class BookDao extends Dao<Book> {

    private static Logger LOGGER = LoggerFactory.getLogger(BookDao.class);

    private String sqlBookSave;

    @Override
    public Book save(Book book) throws DatabaseException {
        int[] types = new int[] {Types.VARCHAR, Types.VARCHAR, Types.BIGINT, Types.BIGINT};
        Object[] data = new Object[] {book.getTitle(), book.getSubTitle(),
                book.getAuthor().getId(), book.getPublishingHouse().getId()};
        Book fullRecord = null;

        try {
            if (getJdbcTemplate().update(sqlBookSave, data, types) > 0) {
                LOGGER.info("Livro salvo com sucesso no banco de dados!");
            }
        } catch (DataAccessException dae) {
            LOGGER.error("Problemas encontrados ao tentar persistir o o livro de título {}",book.getTitle());
            throw new DatabaseException("Problemas encontrados ao tentar persistir o livro de título " + book.getTitle());
        }
        return fullRecord;
    }

    @Override
    public List<Book> listAll() {
        // Não será implementado no escopo do artigo
        return null;
    }

    @Override
    public Book find(String... criteria) {
        // Não será implementado no escopo do artigo
        return null;
    }

    public void setSqlBookSave(String sqlBookSave) {
        this.sqlBookSave = sqlBookSave;
    }
}
