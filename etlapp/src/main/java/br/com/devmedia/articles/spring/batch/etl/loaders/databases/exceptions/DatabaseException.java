package br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions;

/**
 * Exceção genérica para representar qualquer problema que o ETL venha a ter quando se comunicar com o banco de dados.
 *
 * Created by pedrobrigatto on 7/10/15.
 */
public class DatabaseException extends Exception {

    public DatabaseException(String message) {
        super(message);
    }
}
