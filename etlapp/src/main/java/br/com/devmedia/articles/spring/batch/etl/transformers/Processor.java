package br.com.devmedia.articles.spring.batch.etl.transformers;

import br.com.devmedia.articles.spring.batch.etl.model.Author;
import br.com.devmedia.articles.spring.batch.etl.model.Book;
import br.com.devmedia.articles.spring.batch.etl.model.CsvRecord;
import br.com.devmedia.articles.spring.batch.etl.model.PublishingHouse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;


/**
 * Uma vez que registros CSV já estão disponíveis para o chunk, este processor será responsável por
 * transformar seus dados em um objeto em uma representação mais realista de livros, condizente com
 * o modelo de dados da aplicação.
 *
 * Created by pedrobrigatto on 6/21/15.
 */
public class Processor implements ItemProcessor<CsvRecord, Book> {

    private static Logger LOGGER = LoggerFactory.getLogger(Processor.class);

    /**
     * Realiza uma simples transformação do registro CSV, representando-o através de uma
     * composição em que autores, editoras e livros são associados entre si mas, ao mesmo
     * tempo, representados a partir de tipos de objetos distintos, separados.
     *
     * @param csvRecord O registro CSV a ser processado
     * @return O livro já devidamente representado
     * @throws Exception Em caso de algum problema que venha a ocorrer ao longo do processamento
     */
    public Book process(CsvRecord csvRecord) throws Exception {

        LOGGER.info("Iniciando o processamento do registro CSV reference ao livro {}", csvRecord.getBookTitle());

        Book book = new Book();
        book.setNumberOfPages(csvRecord.getNumberOfPages());
        book.setTitle(csvRecord.getBookTitle());
        book.setSubTitle(csvRecord.getBookSubTitle());

        LOGGER.info("Iniciando a criação de um objeto para representar a editora {}", csvRecord.getPublishingHouseName());
        PublishingHouse publishingHouse = new PublishingHouse();
        publishingHouse.setName(csvRecord.getPublishingHouseName());

        LOGGER.info("Configurando um objeto para representar o autor {}", csvRecord.getAuthorName());

        String[] nameTokens = csvRecord.getAuthorName().split(" ");

        if (nameTokens.length == 2) {
            book.setAuthor(new Author(nameTokens[0], nameTokens[1]));
        } else {
            book.setAuthor(new Author(nameTokens[0]));
        }

        book.setPublishingHouse(publishingHouse);

        LOGGER.info("Livro devidamente processado e pronto para ser devolvido ao chunk");
        return book;
    }
}
