package br.com.devmedia.articles.spring.batch.etl.loaders.services;

import br.com.devmedia.articles.spring.batch.etl.loaders.databases.AuthorDao;
import br.com.devmedia.articles.spring.batch.etl.loaders.databases.BookDao;
import br.com.devmedia.articles.spring.batch.etl.loaders.databases.PublishingHouseDao;
import br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions.DatabaseException;
import br.com.devmedia.articles.spring.batch.etl.model.Book;
import org.springframework.transaction.annotation.Transactional;

/**
 * Gerencia o cadastro de livros a partir da coordenação de todos os objetos de acesso a dados
 * que devem ser envolvidos neste processo.
 * Como objetos que representam livros são complexos e se relacionam com outros tipos de entidades
 * como autores, editoras, é importante que a operação de salvamento de registros em qualquer
 * fontes de dados seja transacional, para garantirmos que o processo seja revertido caso qualquer
 * das etapas falhe. Este é o modo pelo qual garantiremos consistência dos dados.
 *
 * Created by pedrobrigatto on 6/27/15.
 */
public class BookService {

    private BookDao bookDao;
    private AuthorDao authorDao;
    private PublishingHouseDao publishingHouseDao;

    @Transactional(rollbackFor = {DatabaseException.class})
    public Book saveBook(final Book book) throws DatabaseException {
        publishingHouseDao.save(book.getPublishingHouse());
        book.setPublishingHouse(publishingHouseDao.find(book.getPublishingHouse().getName()));
        authorDao.save(book.getAuthor());
        book.setAuthor(authorDao.find(book.getAuthor().getName(), book.getAuthor().getSurname()));
        bookDao.save(book);
        return book;
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    public void setPublishingHouseDao(PublishingHouseDao publishingHouseDao) {
        this.publishingHouseDao = publishingHouseDao;
    }
}
