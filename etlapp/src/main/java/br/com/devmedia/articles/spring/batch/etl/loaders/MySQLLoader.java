package br.com.devmedia.articles.spring.batch.etl.loaders;

import br.com.devmedia.articles.spring.batch.etl.loaders.services.BookService;
import br.com.devmedia.articles.spring.batch.etl.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * À medida que os registros estão prontos, transformados em objetos de negócio, este item writer
 * será responsável por, enfim, salvá-los no banco de dados MySQL para, desta forma, torná-los
 * disponíveis para consumidores de toda ordem (outros sistemas, acesso direto, etc).
 *
 * Created by pedrobrigatto on 6/21/15.
 */
public class MySQLLoader implements ItemWriter<Book> {

    private static Logger LOGGER = LoggerFactory.getLogger(MySQLLoader.class);

    private BookService service;

    /**
     * Salva uma lista de livros já devidamente representados nos domínios da aplicação.
     *
     * @param books A lista de livros a ser escrita no banco de dados relacional
     * @throws Exception Caso qualquer problema ocorra durante a escrita dos objetos
     */
    public void write(List<? extends Book> books) throws Exception {
        LOGGER.info("Iniciando o processo de escrita dos livros no banco de dados da solução ...");
        for (Book eachBook : books) {
            service.saveBook(eachBook);
        }
    }

    public void setService(BookService service) {
        this.service = service;
    }
}
