package br.com.devmedia.articles.spring.batch.etl.extractors;

import br.com.devmedia.articles.spring.batch.etl.model.CsvRecord;
import org.springframework.batch.item.file.FlatFileItemReader;

import java.util.logging.Logger;

/**
 * Lê uma linha de uma arquivo CSV, referente a um livro, e constrói uma representação do mesmo a partir de um objeto
 * CsvRecord. Esta classe, na verdade, só foi criada para demonstrar o comportamento de um item reader. Ela não seria
 * necessária, até porque é possível vermos que o que fazemos é bem pouco:
 *
 * <ul>
 *     <li>Lemos um registro a partir do método doRead() da API do FlatFileItemReader</li>
 *     <li>Imprimimos o conteúdo deste objeto para visualizar o fluxo da operação a partir de arquivos de log.</li>
 * </ul>
 *
 *
 * @author pedrobrigatto
 * @see CsvRecord
 */
public class CsvExtractor extends FlatFileItemReader<CsvRecord> {

    private static Logger LOGGER = Logger.getLogger("CsvExtractor");

    public CsvRecord read() throws Exception {
        CsvRecord record = doRead();
        LOGGER.info(String.format(
                "Reading book with title {} from author {}",
                record.getBookTitle(), record.getAuthorName()));
        return record;
    }
}
