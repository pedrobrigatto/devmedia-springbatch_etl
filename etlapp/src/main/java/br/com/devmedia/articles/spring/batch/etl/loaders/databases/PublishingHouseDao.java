package br.com.devmedia.articles.spring.batch.etl.loaders.databases;

import br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions.DatabaseException;
import br.com.devmedia.articles.spring.batch.etl.model.PublishingHouse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Gerencia toda a comunicação do sistema com o banco de dados, no tocante a registros de editoras.
 *
 * Created by pedrobrigatto on 6/28/15.
 */
public class PublishingHouseDao extends Dao<PublishingHouse> {

    private static Logger LOGGER = LoggerFactory.getLogger(PublishingHouseDao.class);

    private String sqlPubHouseSave;
    private String sqlPubHouseListAll;
    private String sqlPubHouseListByName;

    @Override
    public PublishingHouse save(PublishingHouse object) throws DatabaseException {
        int[] types = new int[] {Types.VARCHAR};
        Object[] data = new Object[] {object.getName()};
        PublishingHouse publishingHouse = null;

        try {
            if (getJdbcTemplate().update(sqlPubHouseSave, data, types) > 0) {
                LOGGER.info("Editora salva com sucesso no banco de dados!");
                publishingHouse = find(object.getName());
            }
        } catch (DataAccessException dae) {
            LOGGER.error("Problemas encontrados ao tentar persistir a editora de nome {}", object.getName());
            publishingHouse = find(object.getName());
        }
        return publishingHouse;
    }

    @Override
    public List<PublishingHouse> listAll() throws DatabaseException {

        List<PublishingHouse> publishingHouses = new ArrayList<PublishingHouse>();

        try {
            List<Map<String, Object>> allRecords = getJdbcTemplate().queryForList(sqlPubHouseListAll);

            for (Map<String, Object> eachRecord : allRecords) {
                publishingHouses.add(new PublishingHouse((Integer)eachRecord.get("id"),(String)eachRecord.get("name")));
            }
        } catch (DataAccessException dae) {
            LOGGER.error("Foi encontrado um problema ao recuperar registros de editoras do banco de dados");
            LOGGER.error("Resumo do problema: {}", dae.getMessage());
            throw new DatabaseException(dae.getMessage());
        }

        return publishingHouses;
    }

    @Override
    public PublishingHouse find(String... criteria) throws DatabaseException {

        PublishingHouse publishingHouse = null;

        try {
            List<Map<String,Object>> rows = getJdbcTemplate().
                    queryForList(sqlPubHouseListByName, new Object[] {criteria[0]}, new int[] {Types.VARCHAR});

            if (!rows.isEmpty()) {
                publishingHouse = new PublishingHouse((Integer) rows.get(0).get("id"), (String) rows.get(0).get("name"));
            }

        } catch (DataAccessException dae) {
            LOGGER.error("Foi encontrado um problema ao recuperar o registro de uma editora de nome {}", criteria[0]);
            LOGGER.error("Resumo do problema: {}", dae.getMessage());
            throw new DatabaseException(dae.getMessage());
        }

        return publishingHouse;
    }

    public void setSqlPubHouseSave(String sqlPubHouseSave) {
        this.sqlPubHouseSave = sqlPubHouseSave;
    }

    public void setSqlPubHouseListAll(String sqlPubHouseListAll) {
        this.sqlPubHouseListAll = sqlPubHouseListAll;
    }

    public void setSqlPubHouseListByName(String sqlPubHouseListByName) {
        this.sqlPubHouseListByName = sqlPubHouseListByName;
    }
}
