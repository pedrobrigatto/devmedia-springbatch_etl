package br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions;

/**
 * Exceção que representa a tentativa de inserção de registros duplicados em um banco de dados.
 *
 * Created by pedrobrigatto on 7/10/15.
 */
public class DuplicateRecordException extends Exception {

    public DuplicateRecordException(String message) {
        super(message);
    }
}
