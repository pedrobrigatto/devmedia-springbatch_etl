package br.com.devmedia.articles.spring.batch.etl.loaders.databases;

import br.com.devmedia.articles.spring.batch.etl.loaders.databases.exceptions.DatabaseException;
import br.com.devmedia.articles.spring.batch.etl.model.Author;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Gerencia toda a comunicação do sistema com o banco de dados, no tocante a registros de autores.
 *
 * Created by pedrobrigatto on 6/28/15.
 */
public class AuthorDao extends Dao<Author> {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorDao.class);

    private String sqlAuthorSave;
    private String sqlAuthorListAll;
    private String sqlAuthorByNameAndSurname;

    @Override
    public Author save(Author object) throws DatabaseException {
        int[] types = new int[] {Types.VARCHAR, Types.VARCHAR};
        Object[] data = new Object[] {object.getName(), object.getSurname()};
        Author author = null;

        try {
            if (getJdbcTemplate().update(sqlAuthorSave, data, types) > 0) {
                LOGGER.info("Autor salvo com sucesso no banco de dados!");
                author = find(object.getName(), object.getSurname());
            }
        } catch (DataAccessException dae) {
            LOGGER.error("Problemas encontrados ao tentar persistir o autor de nome {} e sobrenome {}",
                    object.getName(), object.getSurname());
            author = find(object.getName(), object.getSurname());
        }
        return author;
    }

    @Override
    public List<Author> listAll() throws DatabaseException {
        List<Author> authors = new ArrayList<Author>();

        try {
            List<Map<String, Object>> allRecords = getJdbcTemplate().queryForList(sqlAuthorListAll);

            for (Map<String, Object> eachRecord : allRecords) {
                authors.add(new Author((Integer)eachRecord.get("id"),
                        (String)eachRecord.get("name"), (String)eachRecord.get("surname")));
            }
        } catch (DataAccessException dae) {
            LOGGER.error("Foi encontrado um problema ao recuperar registros de autores do banco de dados");
            LOGGER.error("Resumo do problema: {}", dae.getMessage());
            throw new DatabaseException(dae.getMessage());
        }

        return authors;
    }

    @Override
    public Author find(String... criteria) throws DatabaseException {
        Author author = null;

        try {
            List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sqlAuthorByNameAndSurname,
                    new Object[]{criteria[0], criteria[1]},new int[]{Types.VARCHAR, Types.VARCHAR});

            if (!rows.isEmpty()) {
                author = new Author((Integer) rows.get(0).get("id"),
                        (String) rows.get(0).get("name"), (String) rows.get(0).get("surname"));
            }

        } catch (DataAccessException dae) {
            LOGGER.error("Foi encontrado um problema ao recuperar o registro de uma author de nome {} e sobrenome P{",
                    criteria[0], criteria[1]);
            LOGGER.error("Resumo do problema: {}", dae.getMessage());
            throw new DatabaseException(dae.getMessage());
        }

        return author;
    }

    public void setSqlAuthorSave(String sqlAuthorSave) {
        this.sqlAuthorSave = sqlAuthorSave;
    }

    public void setSqlAuthorListAll(String sqlAuthorListAll) {
        this.sqlAuthorListAll = sqlAuthorListAll;
    }

    public void setSqlAuthorByNameAndSurname(String sqlAuthorByNameAndSurname) {
        this.sqlAuthorByNameAndSurname = sqlAuthorByNameAndSurname;
    }
}
