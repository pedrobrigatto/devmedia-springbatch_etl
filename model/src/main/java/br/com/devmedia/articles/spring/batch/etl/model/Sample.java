package br.com.devmedia.articles.spring.batch.etl.model;

import java.util.Date;

public class Sample extends DomainObject {

    private Book book;
    private Date registrationDate;
    private String generalComments;

    public Sample(Integer id) {
        super(id);
    }

    public Sample(Book book, Integer id) {
        this(id);
        this.book = book;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getGeneralComments() {
        return generalComments;
    }

    public void setGeneralComments(String generalComments) {
        this.generalComments = generalComments;
    }
}
