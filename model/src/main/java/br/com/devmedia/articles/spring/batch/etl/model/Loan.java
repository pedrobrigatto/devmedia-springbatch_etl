package br.com.devmedia.articles.spring.batch.etl.model;

import java.util.Date;

public class Loan extends DomainObject {

    private Sample sample;
    private Date loanDate;
    private Date plannedDevolutionDate;
    private Date actualDevolutionDate;
    private SampleStatus status;

    public Loan() {
        super();
    }

    public Loan(Sample sample, Date loanDate, Date plannedDevolutionDate) {
        this();
        this.sample = sample;
        this.loanDate = loanDate;
        this.plannedDevolutionDate = plannedDevolutionDate;
    }

    public Sample getSample() {
        return sample;
    }

    public void setSample(Sample sample) {
        this.sample = sample;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public Date getPlannedDevolutionDate() {
        return plannedDevolutionDate;
    }

    public void setPlannedDevolutionDate(Date plannedDevolutionDate) {
        this.plannedDevolutionDate = plannedDevolutionDate;
    }

    public Date getActualDevolutionDate() {
        return actualDevolutionDate;
    }

    public void setActualDevolutionDate(Date actualDevolutionDate) {
        this.actualDevolutionDate = actualDevolutionDate;
    }

    public SampleStatus getStatus() {
        return status;
    }

    public void setStatus(SampleStatus status) {
        this.status = status;
    }
}
