package br.com.devmedia.articles.spring.batch.etl.model;

public class Author extends DomainObject {

    private String name;
    private String surname;

    public Author(String name) {
        this.name = name;
    }

    public Author(String name, String surname) {
        this(name);
        this.surname = surname;
    }

    public Author(Integer id, String name) {
        super(id);
        this.name = name;
    }

    public Author(Integer id, String name, String surname) {
        super(id);
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
