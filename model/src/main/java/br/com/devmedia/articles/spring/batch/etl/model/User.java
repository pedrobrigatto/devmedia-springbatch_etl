package br.com.devmedia.articles.spring.batch.etl.model;

import java.util.Date;

public class User extends DomainObject {

    private String username;
    private String name;
    private String surname;
    private Date registration;

    private User() {
        super();
    }

    private User (String username) {
        this();
        this.username = username;
    }

    private User (String name, String username) {
        this(username);
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getRegistration() {
        return registration;
    }

    public void setRegistration(Date registration) {
        this.registration = registration;
    }
}
