package br.com.devmedia.articles.spring.batch.etl.model;

import java.io.Serializable;

public abstract class DomainObject implements Serializable {

    private Integer id;

    public DomainObject() {}

    public DomainObject(Integer id) {
        this();
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
