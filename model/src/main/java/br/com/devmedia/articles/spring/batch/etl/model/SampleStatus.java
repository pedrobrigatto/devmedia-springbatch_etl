package br.com.devmedia.articles.spring.batch.etl.model;

/**
 * Indicates the current status of any sample in the system.
 *
 * @author pedrobrigatto
 */
public enum SampleStatus {

    AVAILABLE("Disponível"), BORROWED("Emprestado"), DELAYED("Empréstimo em atraso");

    private final String printableName;

    SampleStatus(final String printableName) {
        this.printableName = printableName;
    }

    public String getPrintableName() {
        return this.printableName;
    }
}
