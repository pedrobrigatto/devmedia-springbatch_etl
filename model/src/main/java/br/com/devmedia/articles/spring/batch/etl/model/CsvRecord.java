package br.com.devmedia.articles.spring.batch.etl.model;

import java.io.Serializable;

public class CsvRecord implements Serializable {

    private String bookTitle;
    private String bookSubTitle;
    private String authorName;
    private Integer numberOfPages;
    private String publishingHouseName;

    public CsvRecord(){}

    public CsvRecord(String bookTitle, String bookSubTitle, String authorName, Integer numberOfPages, String publishingHouseName) {
        this();
        this.bookTitle = bookTitle;
        this.bookSubTitle = bookSubTitle;
        this.authorName = authorName;
        this.numberOfPages = numberOfPages;
        this.publishingHouseName = publishingHouseName;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookSubTitle() {
        return bookSubTitle;
    }

    public void setBookSubTitle(String bookSubTitle) {
        this.bookSubTitle = bookSubTitle;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getPublishingHouseName() {
        return publishingHouseName;
    }

    public void setPublishingHouseName(String publishingHouseName) {
        this.publishingHouseName = publishingHouseName;
    }
}
