package br.com.devmedia.articles.spring.batch.etl.model;

public class PublishingHouse extends DomainObject {

    private String name;

    public PublishingHouse() {
        super();
    }

    public PublishingHouse(Integer id) {
        super(id);
    }

    public PublishingHouse(Integer id, String name) {
        this(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
